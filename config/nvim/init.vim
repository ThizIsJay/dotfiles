set hidden
set nowrap
set encoding=UTF-8
set fileencoding=UTF-8
filetype plugin on
syntax on
set cmdheight=2
set splitbelow
set splitright
set expandtab
set modifiable 
set pumheight=8
set smartindent
set noshowmode
set nobackup
set termguicolors
set clipboard=unnamedplus
set updatetime=300
set tabstop=2
set number
set nowritebackup
set smartindent
set mouse=a
set spell
set spelllang=nl,en_us
set smartindent
set laststatus=0
let mapleader =" "
au! BufWritePost $MYVIMRC source %

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/Repositories/dotfiles/config/nvim/plugged')
Plug 'arcticicestudio/nord-vim'
Plug 'junegunn/goyo.vim'
Plug 'morhetz/gruvbox'
call plug#end()
colorscheme nord

nnoremap <leader><F1> :colorscheme nord<CR>
nnoremap <leader><F2> :colorscheme gruvbox<CR>
nnoremap <ENTER> :Goyo<CR>

let g:goyo_width = 160
au BufReadPost,BufNewFile *.vim,*.conf,*.c,*.cpp,*.h,*.sh :Goyo
au BufReadPost,BufNewFile *.* :Goyo

function! s:goyo_enter()
        set modifiable
endfunction
