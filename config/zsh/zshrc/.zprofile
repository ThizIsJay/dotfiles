#!/bin/zsh

autoload -U colors && colors
PS1=' %B%F{251}%m  %b%F{245} '
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)
bindkey -v 
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zhistory
